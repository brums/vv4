# coding=utf-8
import sys


def check(my_str):
    """
    Данная функция проверяет состоит ли введенная строка только из букв верхнего регистра.
    Args:
        my_str: Строка, которую ввели.
    Returns:
        Строку, если она прошла проверку.
    Raises:
        ValueError.
    Examples:
        >>> check('ASDFG')
        'ASDFG'

        >>> check('asg')
        Traceback (most recent call last):
            ...
        ValueError: некорректно введенное значение
    """
    if my_str.isalpha() and my_str.isupper():
        return (my_str)
    else:
        raise ValueError("некорректно введенное значение")


def search(my_str):
    """
    Функция ишет подсроку, которая повторяется наибольшее количество раз.
    Args:
        my_str: Строка, которую ввели. Она должна состоять только из зашлавных  букв.
    Returns:
        t: подсрока, которая повторяется наибольшее количество раз
    Examples:
        >>> search('ASDASDFGY')
        'ASD'
    """
    max = 0
    t = ''
    for i in range(len(my_str) - 4):
        count = 1
        TRG = ''.join(sorted(my_str[i:i + 3]))
        for i in range(len(my_str) - 4):
            list1 = ''.join(sorted(my_str[i + 1:i + 4]))
            if TRG == list1:
                count += 1
            if count > max:
                max = count
                t = TRG
    print(t)
    return t


def main():
    """
    Главная функция программы.
    """
    my_str = input('введите строку: ')
    check(my_str)
    print(search(my_str))


if __name__ == "__main__":
    main()




