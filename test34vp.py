# coding=utf-8
import pytest
from lab3vp import search
from lab3vp import check

@pytest.mark.parametrize("my_str, res", [('ABBBABABCD', 'ABB'),('AAAAB','AAA')])
def test_search(my_str, res):
    """
    Тест проверяет правильный ли ответ выводит функция при допустимых значениях.
    """
    assert search(my_str) == res

@pytest.mark.parametrize('my_str',['5','h7','!yg(','asd','-=#'])
def test_value_check(my_str):
    """
     Тест проверяет выбрасывает ли функция исключение при вводе недопустимых значений.
    """
    with pytest.raises(ValueError):
        check(my_str)

@pytest.mark.parametrize("my_str", ['ABBBABABCD','FGHD'])
def test_check(my_str):
    """Тест проверяет возвращает ли функция введенную строку, если она удовлетворяет условиям."""
    assert check(my_str) == my_st
